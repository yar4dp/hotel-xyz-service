<?php

namespace App\Traits;


use App\User;
use Illuminate\Http\Request;

trait Registration
{

    /**
     * Register a new user.
     *
     * @param Request $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    private function registerUser(Request $request)
    {
        return User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
        ]);
    }

    /**
     * Validator array for register request.
     *
     * @return array
     */
    private function registerUserValidation()
    {
        return [
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required',
            'role' => 'required|in:customer,employee',
        ];
    }

}