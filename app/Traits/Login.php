<?php

namespace App\Traits;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

trait Login
{
    /**
     * Determine user's role by their email.
     *
     * @param $email
     * @return String
     */
    private function userRole($email)
    {
        $user = User::where('email', $email)->first();
        return $user->role;
    }

    /**
     * Authenticate a login request.
     *
     * @param Request|\Illuminate\Http\Request $request
     * @param $role
     * @return \Illuminate\Http\JsonResponse
     */
    private function auth(Request $request, $role)
    {
        $request->request->add(['role' => $role]);

        $validator = Validator::make($request->all(), $this->loginValidator());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        if ($this->userRole($request->email) != $role) {
            return $this->unauthorized('Invalid login.');
        }

        $user = User::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->password)) {
            $user->token = bcrypt(str_random(32));
            $user->save();

            $data = new \stdClass();
            $data->token = $user->token;
            return $this->success($data, $role . ' logged in.');
        }

        return $this->unauthorized('Invalid login.');
    }

    /**
     * Validator array for login request.
     *
     * @return array
     */
    private function loginValidator()
    {
        return [
            'email' => 'required|exists:users',
            'role' => 'required',
            'password' => 'required',
        ];
    }

}