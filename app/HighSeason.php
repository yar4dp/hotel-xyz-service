<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighSeason extends Model
{
    protected $fillable = ['calendar_date', 'additional_rate'];
}
