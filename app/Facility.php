<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $fillable = ['name', 'description', 'image_url'];

    protected $hidden = ['id', 'created_at', 'updated_at'];
}
