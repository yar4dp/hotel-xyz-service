<?php

namespace App\Http\Controllers;

use App\Facility;
use App\Response\Response;
use Illuminate\Http\Request;
use Validator;

class FacilityController extends Controller
{
    use Response;

    /**
     * FacilityController constructor.
     */
    public function __construct()
    {
        $this->middleware('superuser')->only(['store', 'show', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Facility::all(), 'Facility list.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeValidation());

        if ($validator->fails())
        {
            return $this->badRequest($validator->errors()->first());
        }

        $facility = Facility::create($request->all());

        return $this->success($facility, 'Facility created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Facility $facility)
    {
        return $this->success($facility, 'Facility detail.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Facility $facility)
    {
        $facility->update($request->all());
        return $this->success($facility, 'Facility updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Facility $facility)
    {
        $facility->delete();
        return $this->success($facility, 'Facility removed.');
    }

    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidation()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'image_url' => 'required|url',
        ];
    }
}
