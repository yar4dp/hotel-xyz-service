<?php

namespace App\Http\Controllers;

use App\Response\Response;
use App\Room;
use Illuminate\Http\Request;
use Validator;

class RoomController extends Controller
{
    use Response;

    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        $this->middleware('superuser')->only(['store', 'show', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Room::all(), 'Room list.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $room = Room::create($request->all());

        return $this->success($room, 'Room created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Room $room)
    {
        return $this->success($room, 'Room detail.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Room $room)
    {
        $room->update($request->all());
        return $this->success($room, 'Room updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return $this->success($room, 'Room removed.');
    }

    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidation()
    {
        return [
            'name' => 'required|unique:rooms|max:255',
            'description' => 'required',
            'size' => 'required',
            'bed_type' => 'required',
            'view' => 'required',
            'rate' => 'required',
            'image_url' => 'required',
        ];
    }
}
