<?php

namespace App\Http\Controllers;

use App\Response\Response;
use App\Restaurant;
use Illuminate\Http\Request;
use Validator;

class RestaurantController extends Controller
{
    use Response;

    /**
     * RestaurantController constructor.
     */
    public function __construct()
    {
        $this->middleware('superuser')->only(['store', 'show', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Restaurant::all(), 'Restaurant list.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $restaurant = Restaurant::create($request->all());

        return $this->success($restaurant, 'Restaurant created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Restaurant $restaurant)
    {
        return $this->success($restaurant, 'Restaurant detail.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Restaurant $restaurant)
    {
        $restaurant->update($request->all());
        return $this->success($restaurant, 'Restaurant updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Restaurant $restaurant)
    {
        $restaurant->delete();
        return $this->success($restaurant, 'Restaurant removed.');
    }

    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidation()
    {
        return [
            'name' => 'required|unique:restaurants|max:255',
            'description' => 'required',
            'house_of_operation' => 'required|max:255',
            'dress_code' => 'required|max:255',
            'how_to_reserve' => 'required|max:255',
            'image_url' => 'required|max:255|url',
        ];
    }
}
