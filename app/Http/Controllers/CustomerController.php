<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Response\Response;
use App\Traits\Registration;
use App\Traits\Login;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class CustomerController extends Controller
{
    use Response;
    use Login;
    use Registration;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('customer')->only(['profile', 'delete']);

        $this->middleware('superuser')->only(['delete', 'index', 'store', 'show', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Customer::paginate(), 'Customer list.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->request->add(['role' => 'customer']);

        $validator = Validator::make($request->all(), $this->storeValidator());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $user = $this->registerUser($request);

        $request->request->add(['user_id' => $user->id]);

        $customer = Customer::create($request->all());

        if ($customer) {
            return $this->login($request);
        }

        return $this->badRequest($customer, 'Failed to login.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Customer $customer)
    {
        return $this->success($customer, 'Customer detail.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->user()->update($request->all());
        $customer->update($request->all());

        return $this->success($customer, 'Customer updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Customer $customer)
    {
        $customer->user()->delete();
        $customer->delete();

        return $this->success($customer, 'Customer removed.');
    }

    /**
     * Login as a customer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        return $this->auth($request, 'customer');
    }

    /**
     * Get customer profile by token.
     *
     * @return JsonResponse
     */
    public function profile()
    {
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        return $this->show($customer);
    }

    /**
     * Register a new customer.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        return $this->store($request);
    }

    /**
     * Delete a customer.
     * @return JsonResponse
     */
    public function delete()
    {
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        return $this->destroy($customer);
    }
                                                                                     
    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidator()
    {
        return array_merge($this->registerUserValidation(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'birth_date' => 'required|date_format:"Y-m-d"',
            'address' => 'required',
            'phone_number' => 'required|max:30',
        ]);
    }

}
