<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Response\Response;
use App\Traits\Registration;
use App\Traits\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class EmployeeController extends Controller
{
    use Response;
    use Login;
    use Registration;

    /**
     * EmployeeController constructor.
     */
    public function __construct()
    {
        $this->middleware('employee')->only(['profile', 'delete']);

        $this->middleware('superuser')->only(['index', 'store', 'show', 'update', 'destroy', 'register', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Employee::paginate(), 'Employee list.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->request->add(['role' => 'employee']);

        $validator = Validator::make($request->all(), $this->storeValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $user = $this->registerUser($request);

        $request->request->add(['user_id' => $user->id]);

        $employee = Employee::create($request->all());

        return $this->success($employee, 'Employee created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Employee $employee)
    {
        return $this->success($employee, 'Employee detail.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->user()->update($request->all());
        $employee->update($request->all());

        return $this->success($employee, 'Employee updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Employee $employee)
    {
        $employee->user()->delete();
        $employee->delete();

        return $this->success($employee, 'Employee removed.');
    }

    /**
     * Login as an employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        return $this->auth($request, 'employee');
    }

    /**
     * Get employee profile by token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        $employee = Employee::where('user_id', Auth::user()->id)->first();
        return $this->show($employee);
    }

    /**
     * Register a new employee.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->store($request);
        return $this->login($request);
    }

    /**
     * Delete an employee.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        $employee = Employee::where('user_id', Auth::user()->id)->first();
        return $this->destroy($employee);
    }

    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidation()
    {
        return array_merge($this->registerUserValidation(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone_number' => 'required|max:30',
        ]);
    }

}
