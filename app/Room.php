<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'description', 'size', 'bed_type', 'view', 'rate', 'image_url'];

    protected $hidden = ['created_at', 'updated_at'];
}
