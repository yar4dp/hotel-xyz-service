<?php

namespace App\Response;

class ResponseModel
{
    public $status;
    public $message;
    public $data;
}
