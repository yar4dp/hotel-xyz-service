<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $fillable = ['status', 'note', 'created_by'];

    public function createdBy()
    {
        return $this->belongsTo('App\Employee', 'created_by');
    }

    public function reservation()
    {
        return $this->hasOne('App\Reservation');
    }
}
