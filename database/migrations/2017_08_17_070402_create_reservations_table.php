<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('approval_id')->nullable();
            $table->unsignedInteger('total_person');
            $table->dateTime('check_in');
            $table->dateTime('check_out');
            $table->double('estimated_rate');
            $table->string('guest_first_name');
            $table->string('guest_last_name');
            $table->text('guest_address');
            $table->string('guest_phone_number');
            $table->text('additional_request')->nullable();
            $table->foreign('created_by')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('approval_id')->references('id')->on('approvals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
