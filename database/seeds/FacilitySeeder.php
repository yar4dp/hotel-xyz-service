<?php

use App\Facility;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Facility::create([
            'name' => 'Weddings',
            'description' => '<p>XYZ Jakarta is where “happily-ever-after” begins in grand style. From the Ballroom\'s exquisite details, sophisticated interiors and adjacent VIP room, brides and their grooms have a wealth of elegant options for their special day.</p><p>Intimate or grand, traditional or avant-garde, XYZ Jakarta is the premier choice for your wedding.</p><p>Whether you opt for a gourmet meal followed by dancing or a cocktail reception with innovative hors d’oeuvres, XYZ Jakarta’s talented culinary team exceeds expectations. Our exclusive partner, One Heart Wedding, offers the highest level of bespoke services and wedding concepts unique only to XYZ Jakarta, to create the day of your dreams.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_01.jpg',
        ]);
        Facility::create([
            'name' => 'Meetings',
            'description' => '<p>Reflecting the expectations of a world-class city, XYZ Jakarta is proud to offer professional meeting facilities supported by a comprehensive array of professional and state-of-the-art services in our Senayan Jakarta meeting and event venues. Our event planners offer an expert’s knowledge in planning meetings that are unparalleled successes.</p><p>The hotel’s meeting venues at the gateway to the lush, lively business district of Senayan, Jakarta include flexible meeting rooms, dedicated boardrooms, deluxe guest rooms and suites, a magnificent ballroom featuring a spacious foyer and dedicated arrival and parking for over 700 cars, plus a VIP Meeting Room that is ideal for high level meetings or sensitive negotiation sessions. Designed to host an executive meeting for 10 or a professional conference for 500, we make certain everything you need is at your fingertips.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_02.jpg',
        ]);
        Facility::create([
            'name' => 'Spa',
            'description' => '<p>A sanctuary right in the heart of Jakarta. Willow Stream Spa is among the city’s most luxurious spa retreats. A haven where you can slow down, catch your breath, revitalize and indulge in a calming experience to restore your energy in one of the 9 fully-equipped treatment rooms.</p><p>Designed to promote inner peace and serenity, our Jakarta day spa boasts 900 square meters of soothing spaces and a wide range of natural therapies and treatments that allow you to relax and truly indulge the body, mind and spirit.  </p><p>Every element of your Jakarta luxury spa experience – from the spacious relaxation areas, to the well-appointed spa rooms – will remind you to take time and enjoy life’s simple pleasures.</p> <p>In addition to our Willow Stream Spa, we also offer exclusive benefits for our Health Club members and in-house guests, where they can enjoy the Fitness Center, offering the latest fitness machines, state-of-the-art equipment and a Fitness Studio, as well as the outdoor swimming pool, children’s splash pool and whirlpool to round off your wellness experience.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_03.jpg',
        ]);
        Facility::create([
            'name' => 'Business Services',
            'description' => '<p>XYZ Jakarta offers a full range of business services to our guests and to residents of Senayan, including high-speed wireless internet access throughout the hotel, as well as the Lobby Lounge, Sapori Deli & Cafe, pool side and at the health club.</p><p>Completing the Senayan district, we offer state-of-the-art business services available through our Business Centre, designed to meet and exceed the needs of busy executives. Whether you’re attending a XYZ Jakarta convention or simply taking a working vacation, our Business Centre offers everything you need to be productive, including 3 well-appointed boardrooms, computer stations with high-speed Internet access, secretarial, courier, printing, photocopying and facsimile services, and also IT Support.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_04.jpg',
        ]);
        Facility::create([
            'name' => 'Fitness Gym',
            'description' => '<p>Adjacent to Willow Stream Spa and our inviting outdoor pool is the 625 square meter state-of-the-art gym offering a stunning view of the 4th floor roof top gardens. It features the latest Technogym equipment and a full team of trained fitness professionals.</p><p>Dedicated to promoting a healthy lifestyle, our fitness centre colleagues welcome you to ask questions, try new exercises and experience all that our health centre has to offer.</p><p>The Fitness Centre is open 24 hours. Personal trainers are available to assist from 7:00 a.m. - 5:00 p.m.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_05.jpg',
        ]);
        Facility::create([
            'name' => 'Pool',
            'description' => '<p>Among our many amenities is a fourth-floor outdoor swimming pool with sun terrace and leisure facilities, where a full range of services are available. Pool towels can be obtained from the attendants.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_06.jpg',
        ]);
        Facility::create([
            'name' => 'Underground Passage to Office Towers & Luxury Retail',
            'description' => '<p>The hotel has a private underground walkway that provides guests with direct access to Sentral Senayan I, II, & III office towers & to the luxury retail and dining offerings of Plaza Senayan. This exclusive passageway is located in basement 1 and is open from 6:00 a.m. to 11:00 p.m. daily.</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_07.jpg',
        ]);
        Facility::create([
            'name' => 'Sunrise Art Gallery & Arcade',
            'description' => '<p>Sunrise Art Gallery exhibits both burgeoning as well as renowned international and local artworks which focused primarily on contemporary art. Our sister store Sunrise Arcade features beautiful Indonesian and Japanese handicrafts, accessories as well as smaller artworks that customers will love.</p><p>Location: Level 2, XYZ Jakarta</p><p><b>Hours of Operation:  </b><br/>08:00 – 20:00  </p><p><b> Dress Code: </b><br/> Smart Casual   </p><p><b>Reservations: </b><br/> For more information, please call +622129039496</p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_08.jpg',
        ]);
        Facility::create([
            'name' => 'Paris Miki',
            'description' => '<p>Paris Miki, the biggest optical chain store in Japan, opened its flagship shop in Indonesia at XYZ Jakarta. With more than 960 stores established in Europe, Asia, Australia and the USA, Paris Miki is known for providing unparalleled service and expert advice. Find exclusive collection of Paris Miki House Brand AU (gold and combination eyewear), Micro Titan, and other premium Japanese brands including Ken Okuyama, Kanzaburo Nakamura, Hamamoto and many more.</p><p>Hotels guests can enjoy a number of privileges, including:</p><ul><li>Ultrasonic Cleaning – to make your glasses looks and feels fresh</li><li>General Nose Pad Replacement – to keep your glasses hygienic and comfortable</li><li>Frame Adjustment - to maintain a good stability of the glasses</li><li>Eye Examination – provide consultation for your vision performance</li></ul><p><b>Hours of Operation: </b><br/> 10:00am - 8:00pm </p><p>  <b>Dress Code: </b><br/> Not required   </p><p><b>Reservations:  </b><br/>For more information, please call +62 (21) 2903 9163. </p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_09.jpg',
        ]);
        Facility::create([
            'name' => 'Vita Vino',
            'description' => '<p>The newest addition at XYZ Jakarta, Vita Vino is a wine shop that combines the passion of wine and art. It boasts an extensive range of fine wines as well as Sake and Spirits of great quality and price. Each guest can expect to receive the highest service from the specialist that will help them to select the perfect wine for any occasion. The shop also hosts a range of tasting evening every week.</p><p>Don’t miss their special offers from Buy 1 Get 1 or 15% for certain products during this opening period while stocks last.</p><p>Location: Level B1, XYZ Jakarta</p><p><b>Hours of Operation:  </b><br/>10:00 - 22:00 </p><p>  <b>Dress Code:  </b><br/>Smart casual   </p><p><b>Reservations: </b> <br/>For more information or reservations, please call 021-29703333 ext.3097 </p>',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/facility/facility_10.jpg',
        ]);
    }
}
