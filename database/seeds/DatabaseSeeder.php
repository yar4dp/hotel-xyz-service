<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmployeeSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(RoomSeeder::class);
        $this->call(FacilitySeeder::class);
        $this->call(RestaurantSeeder::class);
        $this->call(HighSeasonSeeder::class);
    }
}
