<?php

use App\HighSeason;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HighSeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-08-31'), // Idul Adha -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-09-01'), // Idul Adha
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-09-02'), // Idul Adha +1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-09-20'), // Muharram -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-09-21'), // Muharram
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-09-22'), // Muharram +1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-11-30'), // Maulid Nabi -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-01'), // Maulid Nabi
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-02'), // Maulid Nabi +1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-21'), // Hari ibu -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-22'), // Hari ibu
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-23'), // Hari ibu +1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-24'), // Natal -1
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-25'), // Natal
            'additional_rate' => 0.30,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-26'), // Natal +1
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-27'), // Awal libur akhir tahun
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-28'), // Libur akhir tahun
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-29'), // Libur akhir tahun
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-30'), // Akhir liburan akhir tahun
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-31'), // Malam pergantian tahun
            'additional_rate' => 0.30,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-01'), // Tahun baru
            'additional_rate' => 0.30,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-02'), // Awal liburan awal tahun
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-03'), // Akhir liburan awal tahun
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-02-15'), // Tahun baru China -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-02-16'), // Tahun baru China
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-02-17'), // Tahun baru China +1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-03-16'), // Nyepi -1
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-03-17'), // Nyepi
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-03-18'), // Nyepi +1
            'additional_rate' => 0.10,
        ]);
    }
}
