<?php

use App\Restaurant;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restaurant::create([
            'name' => 'Spectrum',
            'description' => '<p>Spectrum is the hotel’s all-day-dining restaurants, offering multi-cultural cuisine served on its ‘Culinary Theater’ stations and buffet spread. A contemporary restaurant for breakfast, lunch and dinner with a lively atmosphere.</p>
<p>The Sunday Brunch is one of the best in the city, offering extended buffet spread, including pop-up stations serving brunch favorites like eggs benedict, foie gras, waffles and pancake cooked to order, seafood bar as well as pass around dishes to your table.</p>',
            'house_of_operation' => '06:00 - 10:30 (Breakfast)<br/>
12:00 - 14:30 (Lunch)<br/>
18:00 - 22:00 (Dinner)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Recommended',
            'menu_list_url' => null,
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_01.jpg',
        ]);
        Restaurant::create([
            'name' => 'K22 Bar',
            'description' => '<p>Offering spectacular views of the Jakarta city skyline with service that is meticulous and unobtrusive, K22 exudes an ambience that is dynamic, chic and stylish. This enticing bar offers creative ‘Culinary Cocktails’ and unique presentations. We bring all the essence of the kitchen to the glass; with house made infusions, fresh juices, muddled fruit, infused syrups, earthy spices and savory herbs. Our mixologist creates bespoke cocktails which become an integral part of the atmosphere.</p>
<p>Once night descends the space evokes the elements of sounds – vision – style in a refined lounge concept where a magnetic vibe allows you to chill for hours by the outdoor space while taking in the view of the bustling city of Jakarta.</p>',
            'house_of_operation' => '17:00 – 01:00 (Monday - Sunday)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Required',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-k22-menu-nov-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_02.jpg',
        ]);
        Restaurant::create([
            'name' => 'Peacock Lounge',
            'description' => '<p>The hotel’s lobby will offer a sophisticated lounge where guests can relax and enjoy light bites, tea and coffee throughout the day. Building on XYZ’s tradition of celebrating Afternoon Tea, the menu offerings and service are unrivalled in Jakarta.</p>',
            'house_of_operation' => '9:00 – 21:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Suggested but not required',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-peacock-menu-2017/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_03.jpg',
        ]);
        Restaurant::create([
            'name' => 'Barong Bar',
            'description' => '<p>Barong Bar delivers classics perfected cocktails, masterfully crafted using locally sourced ingredients mixed with professional techniques. A sleek yet relaxed ambience with emphasis on lounging and socializing — an enticing location where guests and local residents enjoy spending an evening drinking with friends in an upmarket yet animated atmosphere.</p> 
<p>The menu offers creative, simple, high quality dishes that can either be served individually or for sharing. These dishes have been created to embrace the cocktail offering.</p>',
            'house_of_operation' => '15:00 – 01:00 (Monday - Friday)<br/>
17:00 – 01:00 (Saturday - Sunday)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Suggested but not required',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-barong-bar-menu-sep-2015/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_04.jpg',
        ]);
        Restaurant::create([
            'name' => 'Sapori Deli',
            'description' => '<p>This exciting dining option offers an Italian themed urban café where various components overlap; barista, gourmet retail, bakery & bistro. A meeting place, reading place, and connecting place with high technology to enable ease of doing business and interaction with colleagues and friends in an urban and relaxed atmosphere. Allowing you to stay connected, maximize your time, and enjoy the company of likeminded guests.</p>',
            'house_of_operation' => 'Deli opens from 06:30 - 20:00<br/>
Restaurant opens from 12:00 - 22:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Not required',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-sapori-restaurant-menu-nov-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_05.jpg',
        ]);
        Restaurant::create([
            'name' => 'View Restaurant & Bar',
            'description' => '<p>VIEW Restaurant & Bar presents modern European cuisine with Asian influences using the best of locally sourced produce.  Led by Chef Hans Christian, the restaurant offers an exquisite a la carte selection where every presentation is to please the eyes and taste buds of the beholder.</p>
<p>Inspired by its name, Chef Hans also prepare a tasting set for guest to sample the best with the Chef’s Point Of View set menu.</p>
<p>Elevate your dining experience with our collection of wines and culinary cocktails while taking in the superb city view of Jakarta, 22 floors above the ground.</p>
<p>Everything just tastes better up here...</p>',
            'house_of_operation' => '17:00 - 23:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Required',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-view-menu-aug-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_06.jpg',
        ]);
        Restaurant::create([
            'name' => 'Motion Blue',
            'description' => '<p>Motion Blue Jakarta is a live music restaurant that offers luxurious yet relaxing atmosphere. Situated on the third floor of XYZ Jakarta, Motion Blue Jakarta delivers an excellent dining experience through delectable European – Japanese fusion cuisine coupled with high quality entertainment.</p>
<p> Partnered with Blue Note, Tokyo’s number one jazz music venue, Motion Blue Jakarta showcases both local and well-known international artists that satisfy its audiences.  It is the rendezvous for sophisticated adults who seek for an authentic and elevated experience.</p>',
            'house_of_operation' => 'Monday to Saturday from 06:00 PM to 12:30 AM <br/>
Closed on Sunday',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Call +62 (21) 2903 9189 or go to www.motionbluejakarta.com',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-motion-blue-menu-sep-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_07.jpg',
        ]);
        Restaurant::create([
            'name' => '1945',
            'description' => '<p>1945 is an innovative re-imagining of traditional Indonesian food as gourmet cuisine. Using precision modern cooking techniques and fresh, premium quality natural ingredients, 1945 strives to create a fine dining experience that is recognizably and authentically Indonesian.</p>
<p>Elevating familiar dishes into exquisitely flavorful and colorful creations through the alchemy of its kitchen, the recipes created by 1945 juxtapose the old and new, the common and the unexpected of Indonesian cooking.</p>',
            'house_of_operation' => '11:00 - 22:00 (Lounge) <br/>
11:00 - 15:30 (Lunch)<br/>
 18:30 - 22:00 (Dinner)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'For reservations, pelase call +62 21 2903 9179 or email reservation@1945.co.id',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-1945-menu-jun-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_08.jpg',
        ]);
        Restaurant::create([
            'name' => 'Senshu',
            'description' => '<p>Senshu offers both the freshest, most delicious Japanese cuisine in a wonderfully serene setting. The menu is crafted true to Japanese culinary tradition from its stunning Teppanyaki, Sushi and Kappo live stations as well as its splendid private dining rooms. Led by Executive Chef Matsuo Yoshiaki, be delighted with the diverse range of Japanese cuisine offered including Teishoku, Omasake Kaiseki, Sushi and Teppanyaki.</p>',
            'house_of_operation' => 'Lunch : 11.30 – 14.00 (last order) <br/>
Dinner : 18.00 – 22.00 (last order)',
            'dress_code' => 'Formal or smart casual',
            'how_to_reserve' => 'For reservations, please call +62 (21) 29039501/03',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-senshu-teppanyaki-menu-jun-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_09.jpg',
        ]);
        Restaurant::create([
            'name' => 'House of Yuen',
            'description' => '<p>Enlivening the taste of Sun Tung Lok – Hong Kong’s Michelin-starred gastronomic haven, House of Yuen reintroduces its distinguishing excellence with a contemporary twist. With a wide, delectable array of Cantonese delicacies, we aim to be synonymous with a constantly exceptional quality and service. Our highly-trained staff and personalized service awaits to suit your needs of the moment. In House of Yuen, find the balance of refined taste and experience that enhances one another.</p>
<p>Nestled in an exquisite setting at XYZ Jakarta in the strategic Senayan Square compound, it is ready to dish out to over 250 guests from business associates as well as families. The restaurant also accommodates 12 private rooms for the city’s diners who wish to banquet comfortably for special occasions.</p>',
            'house_of_operation' => '<p><b>Lunch:  </b><br/>
Monday to Saturday - 11:00am to 3:00pm <br/>
Sunday - 10:00am to 3:00pm<br/></p>
<p><b>Dinner:  </b><br/>
Monday to Sunday - 6:00pm to 11:00pm</p>',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'For reservations or private function inquiries, please call +62 (21) 2903 9172',
            'menu_list_url' => 'http://www.fairmont.com/jakarta/pdf/jak-house-of-yuen-menu-jun-2016/',
            'image_url' => 'http://faizal-hotel-xyz.96.lt/image/restaurant/resto_10.jpg',
        ]);
    }
}
