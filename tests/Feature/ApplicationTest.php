<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTest extends TestCase
{
    private $email = 'littleflower@example.org';
    private $password = 'hanakoizumi';

    public function testRoomListRetrieved()
    {
        $response = $this->json('GET', '/api/room', [], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data',
        ]);

        $this->assertNotNull($response->json()['data']);
    }

    public function testRestaurantListRetrieved()
    {
        $response = $this->json('GET', '/api/restaurant', [], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data',
        ]);

        $this->assertNotNull($response->json()['data']);
    }

    public function testFacilityListRetrieved()
    {
        $response = $this->json('GET', '/api/facility', [], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data',
        ]);

        $this->assertNotNull($response->json()['data']);
    }

    /**
     * Customer registration test.
     *
     * @return String
     */
    public function testCustomerCanRegister()
    {
        $faker = \Faker\Factory::create();
        $firstName = $faker->firstName;

        $response = $this->json('POST', '/api/customer/register', [
            'email' => $this->email,
            'password' => $this->password,
            'first_name' => $firstName,
            'last_name' => $faker->lastName,
            'birth_date' => $faker->date('Y-m-d'),
            'address' => $faker->address,
            'phone_number' => $faker->phoneNumber,
        ], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'token',
            ]
        ]);

        $this->assertDatabaseHas('users', [
            'email' => $this->email,
            'role' => 'customer',
        ]);

        $this->assertDatabaseHas('customers', [
            'first_name' => $firstName,
        ]);

        return $response->json()['data']['token'];
    }

    /**
     * Customer automatically login after register test.
     *
     * @depends testCustomerCanRegister
     * @param $token
     */
    public function testCustomerCanLoginAutomaticallyAfterRegister($token)
    {
        $this->assertDatabaseHas('users', [
            'token' => $token,
        ]);
    }

    /**
     * Customer login test.
     *
     * @depends testCustomerCanRegister
     * @return String
     */
    public function testCustomerCanLogin()
    {
        $response = $this->json('POST', '/api/customer/login', [
            'email' => $this->email,
            'password' => $this->password,
        ], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'token'
            ],
        ]);

        $token = $response->json()['data']['token'];

        $this->assertStringStartsWith('$2y$10$', $token);

        return $token;
    }

    /**
     * Customer profile test.
     *
     * @depends testCustomerCanLogin
     * @param $token
     * @return String
     */
    public function testCustomerCanGetTheirProfile($token)
    {
        $response = $this->json('POST', '/api/customer/profile', [], [
            'Authorization' => $token,
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'first_name',
                'last_name',
                'birth_date',
                'address',
                'phone_number',
                'user' => [
                    'email',
                ],
            ],
        ]);

        $email = $response->json()["data"]["user"]["email"];

        $this->assertStringStartsWith($this->email, $email);

        return $token;
    }

    /**
     * @depends testCustomerCanGetTheirProfile
     * @param $token
     */
    public function testCustomerCanCreateReservationWhenMeetConditions($token)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 45; $i++) {

            $check_in = $faker->dateTimeBetween('now', '+2 months');
            $check_out = Carbon::instance($check_in);
            $check_out->addDay(4);

            $response = $this->json('POST', '/api/customer/reservation/create', [
                'room_id' => $faker->numberBetween(1, 5),
                'total_person' => $faker->numberBetween(1, 4),
                'check_in' => $check_in->format('Y-m-d H:i:s'),
                'check_out' => $check_out->format('Y-m-d H:i:s'),
                'guest_first_name' => $faker->firstName,
                'guest_last_name' => $faker->lastName,
                'guest_address' => $faker->address,
                'guest_phone_number' => $faker->phoneNumber,
            ], [
                'x-api-key' => env('API_KEY'),
                'Authorization' => $token,
            ]);

            $check_in_time = $check_in->format('H:i');
            $check_out_time = $check_out->format('H:i');

            if (Carbon::parse('14:00')->lessThanOrEqualTo(Carbon::parse($check_in_time)) &&
                Carbon::parse('12:00')->greaterThanOrEqualTo(Carbon::parse($check_out_time))) {

                $response->assertJson([
                    'status' => 200,
                ]);

                $response->assertJsonStructure([
                    'status',
                    'data' => [
                        'total_person',
                        'check_in',
                        'check_out',
                        'created_by' => [
                            'first_name',
                            'last_name',
                            'user' => [
                                'email',
                            ]
                        ],
                        'room' => [
                            'name',
                            'size',
                        ],
                        'approval',
                    ]
                ]);

            } else {

                $response->assertJson([
                    'status' => 400,
                ]);

            }
        }

        return $token;
    }

    /**
     * Delete test customer, so you can run customer test again.
     *
     * @depends testCustomerCanCreateReservationWhenMeetConditions
     * @param $token
     */
    public function testYouCanRunThisTestAgain($token)
    {
        $response = $this->json('DELETE', '/api/customer/delete', [], [
            'x-superuser-key' => env('SUPERUSER_KEY'),
            'Authorization' => $token,
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => $this->email,
        ]);
    }

}
